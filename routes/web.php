<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('lang/{language}', ['as'=>'lang.switch', 'uses'=>'LanguageController@switchLang']);

Route::group(['middleware'=>'language','prefix'=>Config::get('routeLang')], function(){
    Route::get('/', ['as'=>'home', 'uses'=>'GeneralController@index']);
    Route::get('/page/{name}', ['as'=>'page', 'uses'=>'GeneralController@page']);
    Route::get('/units/{id}', ['as'=>'units', 'uses'=>'GeneralController@unit']);

    Route::post('/calculate/{id}', ['as'=>'calculate', 'uses'=>'GeneralController@calculate']);
    Route::post('/proposal', ['as'=>'proposal', 'uses'=>'GeneralController@proposal']);
});

Route::group(['prefix'=>'dashboard', 'namespace'=>'Dashboard','middleware'=>'auth'],function(){
    //App::setlocale('ru');
    Route::get('/',['as'=>'dashboard', 'uses'=>'DashboardController@index']);
    Route::resource('users', 'UserController', ['except' => 'show']);
    Route::resource('units', 'UnitController', ['except' => 'show']);
    Route::resource('holidays', 'HolidayController', ['except' => 'show']);
    Route::resource('pages', 'PageController', ['except' => 'show']);
    Route::resource('options', 'OptionController', ['except' => 'show']);
    //Route::resource('proposals', 'ProposalController', ['except' => 'show']);
    Route::resource('priceindexes', 'PriceindexController', ['except' => 'show']);
    Route::resource('bankrates', 'BankrateController', ['except' => 'show']);
    Route::resource('minsalaries', 'MinsalaryController', ['except' => 'show']);
    Route::resource('minlifes', 'MinlifeController', ['except' => 'show']);
});

Route::group(['prefix'=>'datatable'],function(){
    Route::post('/getpriceindexes', ['as'=>'getpriceindexes', 'uses'=>'DatatableController@getPriceindexes']);
});

Route::get('/register',function(){
    return redirect(route('login'));
});

