jQuery(document).ready(function($){
    $(window).scroll(function() {
        if($(this).scrollTop() != 0) { 
            $('#GotoTop').fadeIn(); 
        } else { 
            $('#GotoTop').fadeOut(); 
        }
    });
    $('#GotoTop').click(function() {
        $('body,html').animate({scrollTop:0},800);
    });

    $('.mobil-button').click(function(){
        $('.inner-menu').toggleClass('visible');
    });

    $('.btn-remove').click(function(e){
        e.preventDefault();
        $("#remove-form").attr('action',$(this).attr('href'));
    });

    /* Установка класса "Active" для активного меню */
        var url = window.location.origin + window.location.pathname;
        var element = $('aside a').filter(function() {
            return this.href == url;
        }).addClass('active').parent();
});