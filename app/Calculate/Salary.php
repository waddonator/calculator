<?php

namespace App\Calculate;

use App\Minlife;
use App\Priceindex;
use Carbon\Carbon;

class Salary
{

    public function calculate($data)
    {
        # выбираем все периоды начиная от базисного месяца
        $y0 = (int)substr($data->monthbase,3,4);
        $m0 = (int)substr($data->monthbase,0,2);
        $y1 = (int)substr($data->monthstart,3,4);
        $m1 = (int)substr($data->monthstart,0,2);
        $y2 = (int)substr($data->monthfinish,3,4);
        $m2 = (int)substr($data->monthfinish,0,2);
        # меняем даты местами если начало позже конца
        if ($y1>$y2 || ($y1==$y2 && $m1>$m2)){
            $y_temp = $y1; $y1 = $y2; $y2 =  $y_temp;
            $m_temp = $m1; $m1 = $m2; $m2 =  $m_temp;
        }
        # если базисный больше начала, то уменьшаем его
        $y_temp = $y1; $m_temp = $m1 - 1; if($m_temp==0) {$y_temp--; $m_temp+=12;}
        if ($y0>$y_temp || ($y0==$y_temp && $m0>$m_temp)){
            $y0 = $y_temp; $m0 = $m_temp;
        }
        $result  = '<h3 style="text-align:center;">Параметри розрахунку</h3>';
        $result .= '<table width="100%" class="table table-striped mb-5"><tbody>';
        $result .= '<tr><td>Початок періоду</td><td>' . trans('calculator.month'.$m1) . ' ' . $y1 . '</td></tr>';    
        $result .= '<tr><td>Кінець періоду</td><td>' . trans('calculator.month'.$m2) . ' ' . $y2 . '</td></tr>';    
        $result .= '<tr><td>Базовий місяць</td><td>' . trans('calculator.month'.$m0) . ' ' . $y0 . '</td></tr>';    
        $result .= '<tr><td>Сума нарахованої заробітної плати</td><td>' . $data->salary . ' грн.</td></tr>';    
        $result .= '</tbody></table>';    

        # выбираем индексы инфляции
        $elements = Priceindex::where(function ($query) use ($y0, $m0){
            $query->where('year', '>', $y0)
            ->orWhere([['year', '=', $y0],['month','>=',$m0]]);
        })
        ->where(function ($query) use ($y2, $m2){
            $query->where('year', '<', $y2)
            ->orWhere([['year', '=', $y2],['month','<=',$m2]]);
        })
        ->orderby('year','ASC')->orderby('month','ASC')
        ->get();

        $result  .= '<h3 style="text-align:center;">Результат розрахунку</h3>';
        # Добавляем индекс для расчета суммы индексации и период, в котором этот индекс будет применен.
        if (isset($elements) && $elements){
            //$result .= '<table width="100%" class="table table-striped"><thead><tr><th align="center">Період</th><th align="center">Індекс інфляції</th><th align="center">Індекс розрахунку</th><th align="center">Ratio</th><th align="center">Період 2</th><th align="center">Minsalary</th><th align="center">Сумма индексации</th><th align="center">Зарплата с индексацией</th></thead><tboby>';
            $index = 1;
            $ratio = 1;
            $index = 1;
            foreach ($elements as $key => $element) {
                
                if ($key){
                    $index = $element->value * $index / 100;
                    $element->index = $index;
                }

                if ($element->index > 1.03){
                    $ratio *= round($element->index,3);
                    $index = 1;
                }

                if ($ratio != 1) {
                    $element->ratio = round($ratio,3) * 100 - 100;
                } else {
                    $element->ratio = 0;
                }

                $y_temp = $element->year;
                $m_temp = $element->month + 2;
                if ($m_temp>12){$m_temp-=12;$y_temp++;}
                $element->month2 = $m_temp;
                $element->year2 = $y_temp;
                # Выбираем минимальную заработную плату
                $minlife = Minlife::where([['year','<',$y_temp]])->orWhere([['year','=',$y_temp],['month','<=',$m_temp]])->orderby('year','DESC')->orderby('month','DESC')->first();
                $element->minlife = $minlife->value_worker ? $minlife->value_worker : 0;
                # Рассчитываем сумму индексации
                $element->add = min((int)$data->salary,$element->minlife) * $element->ratio / 100;
                //$result .= '<tr data-key="'.$key.'"><td align="center">'.trans('calculator.month'.$element->month) . ' ' . $element->year . '</td><td align="center">' . number_format($element->value,3) . '</td><td align="center">' . number_format($element->index,3) . '</td><td align="center">' . number_format($element->ratio,3) . '</td><td align="center">'.trans('calculator.month'.$element->month2) . ' ' . $element->year2 . '</td><td align="center">' . number_format($element->minlife,2) . '</td><td align="center">' . number_format($element->add,2) . '</td><td align="center">' . number_format($element->add + $data->salary,2) . '</td></tr>';
            }
            //$result .= '</tboby></table>';    
        }

        # Отображаем результат
        if (isset($elements) && $elements){
            $result .= '<table width="100%" class="table table-striped mb-5"><thead><tr><th align="center" class="align-middle">Період розрахунків</th><th align="center" class="align-middle">Коефіцієнт індексації</th><th align="center" class="align-middle">Прожитковий мінімум, грн.</th><th align="center" class="align-middle">Сума індексації, грн.</th><th align="center" class="align-middle">Сума зарплати з індексацією, грн.</th></thead><tbody>';
            foreach ($elements as $key => $element) {
                if(($element->year2 > $y1 || ($element->year2 == $y1 && $element->month2 >= $m1)) && ($element->year2 < $y2 || ($element->year2 == $y2 && $element->month2 <= $m2))){
                    $result .= '<tr><td align="center">'.trans('calculator.month'.$element->month2) . ' ' . $element->year2 . '</td><td align="center">' . number_format($element->ratio,2) . '</td><td align="center">' . number_format($element->minlife,2) . '</td><td align="center">' . number_format($element->add,2) . '</td><td align="center">' . number_format($element->add + $data->salary,2) . '</td></tr>';
                }
            }
            $result .= '</tbody></table>';    
        }
       
        //$result .= json_encode($data);
        return $result;
    }
	
}
