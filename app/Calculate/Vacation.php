<?php

namespace App\Calculate;

use App\Holiday;
use Carbon\Carbon;

class Vacation
{

    public function calculate($data)
    {
        $res_array = [];

                $holidays = [];
                $datestart  = new Carbon($data->datestart); # начало отпуска
                $datetemp = new Carbon($data->datestart);   # временная дата
                $datefinish = new Carbon($data->datestart); # дата окончания отпуска
                $dateout = new Carbon($data->datestart);    # дата выхода
                $daycount = $data->daycount;                # дней отпуска
                $daysadd = 0;                               # кол-во доп. праздничный дней
                $datetemp->addDays(-1);
                $i=0;
                while ($i < $daycount) {
                    $model = Holiday::whereDate('date','=',$datetemp->addDay())->whereIn('type', [0])->first();
                    if ($model){
                        $daysadd++;
                        $holidays[] = $model->name . ' - ' . $model->date->format('d.m.Y');
                    } else {
                        $i++;
                    }
                }
                $datefinish->addDays($daycount + $daysadd - 1);
                # высчитываем дату выхода
                $ok = false;
                $dateout->addDays($daycount + $daysadd - 1);                
                while (!$ok) {
                    $datetemp = $dateout->addDay();
                    $model = Holiday::whereDate('date','=',$datetemp)->whereIn('type', [0])->first();
                    $model2 = Holiday::whereDate('date','=',$datetemp)->whereIn('type', [1])->first();
                    $model3 = Holiday::whereDate('transfer','=',$datetemp)->whereIn('type', [2])->first();
                    if (!$model && !$model2 && !$model3 && $dateout->format('D')!='Sat' && $dateout->format('D')!='Sun'){ $ok = true; }
                    if ($model){
                        $holidays[] = $model->name . ' - ' . $model->date->format('d.m.Y');
                    }
                }
                # формируем результат
                $res_array[] = (object)['name'=>'Дата початку відпустки:', 'value'=>$datestart->format('d.m.Y')];
                $res_array[] = (object)['name'=>'Дата кінця відпустки:', 'value'=>$datefinish->format('d.m.Y')];
                $res_array[] = (object)['name'=>'Дата виходу:', 'value'=>$dateout->format('d.m.Y')];
                $res_array[] = (object)['name'=>'Кількість днів відпустки:', 'value'=>$daycount];
                //$res_array[] = (object)['name'=>'Святкові дні:', 'value'=> '<ul><li>'.implode('</li><li>',$holidays).'</li></ul>'];
                $res_array[] = (object)['name'=>'Святкові дні:', 'value'=> implode('<br>',$holidays)];

        # собираем результат
            if ($res_array){
                // $result = '<ul class="list-group list-group-flush">';
                $result = '<table width="100%" class="table table-striped"><tbody>';
                foreach ($res_array as $key => $row) {
                    // $result .= '<li class="list-group-item"><span class="font-weight-semibold">'.$row->name.'</span><div class="ml-auto">'. $row->value . '</div></li>';            }
                    $result .= '<tr><td class="font-weight-semibold">'.$row->name.'</td><td>'. $row->value . '</td></tr>';            }
                $result .= '</body></table>';
                // $result .= '</ul>';
            } else {
                $result = 'Обробника калькулятора не знайдено';
            }


        return $result;
    }
	
}
