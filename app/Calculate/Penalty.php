<?php

namespace App\Calculate;

use App\Priceindex;
use App\Bankrate;
use Carbon\Carbon;

class Penalty
{

    public function calculate($data)
    {
        if (new Carbon($data->datestart) < new Carbon($data->datefinish)){
            $datestart  = new Carbon($data->datestart);
            $datetemp   = new Carbon($data->datestart);
            $datefinish = new Carbon($data->datefinish);
        } else {
            $datestart  = new Carbon($data->datefinish);
            $datetemp   = new Carbon($data->datefinish);
            $datefinish = new Carbon($data->datestart);
        }
        $days = ($datefinish->diffInDays($datestart)) + 1;
        $result  = '<h3 style="text-align:center;">Сума боргу</h3>';
        $result .= '<p>Основна сума заборгованості за період з <strong>' . $datestart->format("d.m.Y") . ' по ' . $datefinish->format("d.m.Y") . '</strong> складає <strong>' . $data->debt . ' грн.</strong>';

        $result .= '<h3 style="text-align:center;" class="mt-5">Інфляційні витрати</h3>';
        if ($data->check1){
            $ii = [];
            $y1 = (int)$datestart->format("Y");
            $m1 = (int)$datestart->format("m");
            $y2 = (int)$datefinish->format("Y");
            $m2 = (int)$datefinish->format("m");
            $elements = Priceindex::where(function ($query) use ($y1, $m1){
                            $query->where('year', '>', $y1)
                            ->orWhere([['year', '=', $y1],['month','>',$m1]]);
                        })
                        ->where(function ($query) use ($y2, $m2){
                            $query->where('year', '<', $y2)
                            ->orWhere([['year', '=', $y2],['month','<=',$m2]]);
                        })
                        ->orderby('year','ASC')->orderby('month','ASC')
                        ->get();
            $ii2 = 1;
            if ($elements){
                $result .= '<table width="100%" class="table table-striped"><thead><tr><th align="center">Період</th><th align="center">Індекс інфляції</th></thead><tboby>';
                foreach ($elements as $key => $element) {
                    $result .= '<tr><td align="center">'.trans('calculator.month'.$element->month) . ' ' . $element->year . '</td><td align="center">' . number_format($element->value,1) . '</td></tr>';
                    $ii[] = '(' . number_format($element->value,1) . ' : 100)';
                    $ii2 *= ($element->value/100);
                }
                $result .= '</tboby></table>';    
            }
            $result .= '<p><strong>Індекс інфляції за весь період</strong> = ' . implode(' x ', $ii) . ' = <strong>' . number_format($ii2,6) . '</strong></p>';
            $iz = round($data->debt * ($ii2 - 1),2);
            $result .= '<p><strong>Інфляційне збільшення</strong> = ' . $data->debt . ' x ' . number_format($ii2,6) . ' - ' . $data->debt . ' = <strong>' . $iz . ' грн.</strong></p>';
        } else {
            $result .= '<p><strong>Індекс інфляції не враховувався</strong></p>';
            $iz = 0;
        }

        $result .= '<h3 style="text-align:center;" class="mt-5">Штрафні санкції</h3>';
        if ($data->check2){
            $result .= '<p><strong>' . trans('calculator.typepenalty'.$data->typepenalty) . '</strong></p>';
            $ss = 0;
            # Расчет в зависимости от типа
            if ($data->typepenalty == 1){
                $table = [];
                while ($datetemp <= $datefinish) {
                    $element = Bankrate::whereDate('date','<=',$datetemp)->orderby('date','DESC')->first();
                    if (isset($table[$element->date->format("d.m.Y")])){
                        $table[$element->date->format("d.m.Y")]->count++;
                    } else {
                        $table[$element->date->format("d.m.Y")] = (object)["count"=>1,"value"=>$element->value];
                    }
                    $datetemp->addDay();
                }
                $result .= '<table width="100%" class="table table-striped"><thead><tr><th>Початок періода</th><th>Кількість днів</th><th>Ставка НБУ</th><th>Подвійна ставка НБУ на день</th><th>Штрафні санкції</th></tr></thead><tbody>';
                $count = 0;
                foreach ($table as $key => $row) {
                    $row->double = $row->value * 2 / 365;
                    $row->ss = $data->debt * $row->double * $row->count / 100;
                    $count += $row->count;
                    $ss += $row->ss;
                    $ss2[] = round($row->ss,2);
                    $result .= '<tr><td align="center">'.$key.'</td><td align="center">'.$row->count.'</td><td align="center">'.$row->value.'</td><td align="center">'.round($row->double,3).'</td><td align="center">'.round($row->ss,2).'</td></tr>';
                }
                $ss = round($ss,2);
                $result .= '<tr><td align="center"><strong>Разом:</strong></td><td align="center"><strong>'.$count.'</strong></td><td align="center"></td><td align="center"></td><td align="center"><strong>'.$ss.'</strong></td></tr>';
                $result .= '</tbody></table>';
                $result .= '<p><strong>Штрафні санкції</strong> = (сума санкцій по всіх періодах) = ' . implode(' + ',$ss2) . ' = <strong>' . $ss . ' грн.</strong></p>';
            }
            if ($data->typepenalty == 2){
                $ss = round($data->debt * $data->size * $days / 100, 2);
                $result .= '<p><strong>Штрафні санкції</strong> = (сума заборгованості) x (розмір пені, зазначений в договорі) x (кількість днів прострочення) = ' . $data->debt . ' x ' . $data->size . '% x ' . $days . ' = <strong>' . $ss . ' грн.</strong></p>';
            }
            if ($data->typepenalty == 3){
                $ss = round($data->debt * 3 * $days / 365 / 100, 2);
                $result .= '<p><strong>Штрафні санкції</strong> = (сума заборгованості) x 3% x (кількість днів прострочення) : (кількість днів у році) = ' . $data->debt . ' x 3% x ' . $days . ' : 365 = <strong>' . $ss . ' грн.</strong></p>';
            }
            if ($data->typepenalty == 4){
                $ss = round($data->debt * $data->size * $days / 365 / 100, 2);
                $result .= '<p><strong>Штрафні санкції</strong> = (сума заборгованості) x (відсоток річних, зазначений в договорі) x (кількість днів прострочення) : (кількість днів у році) = ' . $data->debt . ' x ' . $data->size . '% x ' . $days . ' : 365 = <strong>' . $ss . ' грн.</strong></p>';
            }
        } else {
            $result .= '<p><strong>Штрафні санкції не застосовано</strong></p>';
            $ss = 0;
        }

        $result .= '<h3 style="text-align:center;" class="mt-5">Загальна заборгованість</h3>';
        $result .= '<table width="100%" class="mb-5"><tbody>';
        $result .= '<tr><td>Період прострочення грошового зобов`язання:</td><td align="center">з ' . $datestart->format("d.m.Y") . ' по ' . $datefinish->format("d.m.Y") . '</td></tr>';
        $result .= '<tr><td>Кількість днів:</td><td align="center">' . $days . '</td></tr>';
        $result .= '<tr><td>Сума боргу:</td><td align="center">' . $data->debt . ' грн.</td></tr>';
        $result .= '<tr><td>Інфляційне збільшення:</td><td align="center">' . $iz . ' грн.</td></tr>';
        $result .= '<tr><td>Штрафні санкції:</td><td align="center">' . $ss . ' грн.</td></tr>';
        $result .= '<tr><td><strong>Разом:</strong></td><td align="center"><strong>' . ($data->debt + $iz + $ss) . ' грн.</strong></td></tr>';
        $result .= '</tbody></table>';
        return $result;
    }
	
}
