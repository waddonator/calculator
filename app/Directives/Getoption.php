<?php
namespace App\Directives;

use App\Option;

class Getoption{

    public function show($obj, $data =[]){
        $result = '';
        if ($obj){
            $element = Option::where('name','=',$obj)->first();
            $result = $element ? $element->value : '';
        }
        return $result;
    }
}