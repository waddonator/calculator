<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Request;
use Config;
use App;

class LanguageServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->setRouteLang();
    }

    public function setRouteLang()
    {
        $language = Request::segment(1);
        $routeLang = '';
        if (isset(config('app.locales')[$language])) {
            App::setLocale($language);
            $routeLang = $language;
        }
        Config::set('routeLang', $routeLang);
    }

}
