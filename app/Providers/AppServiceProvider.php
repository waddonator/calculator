<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use App;
use Blade;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        # Вытаскиваем свойство из таблицы
        Blade::directive('getoption', function ($name='') {
            return "<?php echo app('getoption')->show($name); ?>";
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        App::singleton('getoption', function(){
            return new \App\Directives\Getoption();
        });
    }
}
