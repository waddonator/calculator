<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Priceindex;

class DatatableController extends Controller
{

    public function getPriceindexes(Request $request)
    {
        $columns = [
            0 => 'id',
            1 => 'month',
            2 => 'year',
            3 => 'value',
            4 => 'actions'
        ];
        $totalData = Priceindex::count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value'))){
            $models = Priceindex::offset($start)
                ->orderby($order,$dir)
                ->limit($limit)
                ->get();
                $totalFiltered = $totalData;
        } else {
            $search = $request->input('search.value');
            $models = Priceindex::where('id','like',"%{$search}%")
                ->orWhere('year','like',"%{$search}%")
                ->orWhere('value','like',"%{$search}%")
                ->offset($start)
                ->orderby($order,$dir)
                ->limit($limit)
                ->get();
            $totalFiltered = Priceindex::where('id','like',"%{$search}%")
                ->orWhere('year','like',"%{$search}%")
                ->orWhere('value','like',"%{$search}%")
                ->count();
        }

        $data = [];
        if($models){
            foreach($models as $model){
                $nestedData['id'] = $model->id;
                $nestedData['month'] = trans('calculator.month'.$model->month);
                $nestedData['year']  = $model->year;
                $nestedData['value'] = $model->value;
                $nestedData['actions'] = '
                <div class="btn-group">
                    <a href="'.route('priceindexes.edit',$model->id).'" class="btn btn-info btn-sm" title="Змінити"><i class="fa fa-pencil fa-fw"></i></a>
                    <a href="'.route('priceindexes.destroy',$model->id).'" class="btn btn-danger btn-sm btn-remove" data-toggle="modal" data-target="#removeModal" title="Видалити"><i class="fa fa-trash-o fa-fw"></i></a>
                </div>
                ';
                $data[] = $nestedData;
            }
        }
        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => $data,

        ];
        echo json_encode($json_data);
    }

}
