<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use App\Unit;

class GeneralController extends Controller
{
    public function index()
    {
        $models = Unit::get();
        return view('index',['models'=>$models]);
    }

    public function page($name)
    {
        $model = Page::where('slug','=',$name)->first();
        if ($model){
            return view('page',['model'=>$model]);
        } else {
            return abort(404);
        }
    }

    public function unit($name)
    {
        $model = Unit::where('slug','=',$name)->first();
        if ($model){
            return view('unit',['model'=>$model]);
        } else {
            return abort(404);
        }
    }

    public function calculate($slug)
    {
        $data = isset($_POST['data']) ? json_decode($_POST['data']) : (object)[];
        //$data = isset($_POST['data']) ? $_POST['data'] : [];
        $model = Unit::where('slug','=', $slug)->first();
        if ($model) {
            $result = $model->calculate($data);
        } else {
            $result = 'Model not found';
        }
        return json_encode($result);
    }

    public function proposal()
    {
        $result = 'We got a message';
        return json_encode($result);
    }

}
