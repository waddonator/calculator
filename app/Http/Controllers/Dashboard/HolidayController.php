<?php

namespace App\Http\Controllers\Dashboard;

use App\Holiday;
use App\Http\Requests\StoreHolidayRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HolidayController extends Controller
{
    public function index(Request $request)
    {
        $models = Holiday::where([['trash','=',false]])->orderby('date','DESC')->paginate(20);
        return view('dashboard.holiday.index', [
                'models' => $models,
                'pagination_add' => $request->input(),
            ]);
    }

    public function create(Request $request)
    {
        $types = (object)[trans('calculator.holidaytype0'), trans('calculator.holidaytype1'), trans('calculator.holidaytype2')];
        return view('dashboard.holiday.create', [
                'types' => $types,
            ]);
    }

    public function store(StoreHolidayRequest $request)
    {
        $model = new Holiday;
        $model->name = $request->name;
        $model->date = $request->date;
        $model->type = $request->type;
        $model->transfer = $request->transfer;
        $model->save();
        return redirect()->route('holidays.index')
            ->with('flash_message', trans('cms.alert-create', ['name' => $model->name]));
    }

    public function show($id)
    {
    }

    public function edit($id)
    {
        $types = (object)[trans('calculator.holidaytype0'), trans('calculator.holidaytype1'), trans('calculator.holidaytype2')];
        $model = Holiday::findOrFail($id);
        return view('dashboard.holiday.edit', [
                'model'=>$model,
                'types' => $types,
            ]);
    }

    public function update(StoreHolidayRequest $request, $id)
    {
        $model = Holiday::findOrFail($id);
        $model->name = $request->name;
        $model->date = $request->date;
        $model->type = $request->type;
        $model->transfer = $request->transfer;
        $model->save();
        return redirect()->route('holidays.index')
            ->with('flash_message', trans('cms.alert-update', ['name' => $model->name]));
    }

    public function destroy(Request $request, $id)
    {
        $model = Holiday::findOrFail($id);
        $model->trash = true;
        $model->save();
        //$model->delete();
        return redirect(route('holidays.index'))->with('error_message', trans('cms.alert-remove', ['name' => $model->name]));
    }

}
