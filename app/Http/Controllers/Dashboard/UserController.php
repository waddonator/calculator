<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Requests\StoreUserRequest;

use App\User;
use Auth;
use App\Http\Controllers\Controller;

//Importing laravel-permission models
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

//Enables us to output flash messaging
use Session;

class UserController extends Controller {

    public function index(Request $request) {        
        $models = User::paginate(20);
        return view('dashboard.user.index', [
                'models'=>$models,
                'pagination_add'=>$request->input(),
            ]);
    }

    public function create() {
        return view('dashboard.user.create', [
            ]);
    }

    public function store(StoreUserRequest $request) {
        $model = new User;
        $model->email = $request->input('email');
        $model->name = $request->input('name');
        $model->password = $request->input('password');
        $model->save();

        return redirect()->route('users.index')
            ->with('flash_message', trans('messages.appended', ['item'=>trans('main.user'), 'name' => $model->name]));
    }

    public function show(Request $request, $id)
    {
    }

    public function edit($id) {
        $model = User::findOrFail($id);
        return view('dashboard.user.edit', [
                'model'=>$model,
            ]);
    }

    public function update(Request $request, $id)
    {
        $model = User::findOrFail($id);

        //Validate name, email and password fields
        $validate = [
            'email'=>'required|email|unique:users,email,' . $id,
            'name' => 'required|string|max:255',
        ];
        if ($request->input('password')){
            $validate['password'] = 'required|string|max:255|same:password_confirmation';
            $validate['password_confirmation'] = 'required|string|max:255|same:password';
        }
        $this->validate($request, $validate);

        $model->email = $request->input('email');
        $model->name = $request->input('name');
        if ($request->input('password')){
            $model->password = $request->input('password');
        }
        $model->save();
        
        return redirect()->route('users.index')
            ->with('flash_message', trans('messages.edited', ['item'=>trans('main.user'), 'name' => $model->name]));
    }

    public function destroy(Request $request, $id) {
        $model = User::findOrFail($id);
        $model->delete();
        return redirect(route('users.index'))->with('error_message', trans('messages.deleted', ['item'=>trans('main.user'), 'name' => $model->name]));
    }

}