<?php

namespace App\Http\Controllers\Dashboard;

use App\Unit;
use App\Http\Requests\StoreUnitRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UnitController extends Controller
{
    public function index(Request $request)
    {
        $models = Unit::where([['trash','=',false]])->orderby('id')->paginate(20);
        return view('dashboard.unit.index', [
                'models' => $models,
                'pagination_add' => $request->input(),
            ]);
    }

    public function create(Request $request)
    {
        return view('dashboard.unit.create', [
            ]);
    }

    public function store(StoreUnitRequest $request)
    {
        $model = new Unit;
        $model->name = $request->name;
        $model->slug = $request->slug;
        $model->image = $request->image ? $request->image : '';
        $model->hint = $request->hint ? $request->hint : '';
        $model->visible = $request->visible;
        $model->save();
        return redirect()->route('units.index')
            ->with('flash_message', trans('cms.alert-create', ['name' => $model->name]));
    }

    public function show($id)
    {
    }

    public function edit($id)
    {
        $model = Unit::findOrFail($id);
        return view('dashboard.unit.edit', [
                'model'=>$model,
            ]);
    }

    public function update(StoreUnitRequest $request, $id)
    {
        $model = Unit::findOrFail($id);
        $model->name = $request->name;
        $model->slug = $request->slug;
        $model->image = $request->image ? $request->image : '';
        $model->hint = $request->hint ? $request->hint : '';
        $model->visible = $request->visible;
        $model->save();
        return redirect()->route('units.index')
            ->with('flash_message', trans('cms.alert-update', ['name' => $model->name]));
    }

    public function destroy(Request $request, $id)
    {
        $model = Unit::findOrFail($id);
        $model->trash = true;
        $model->save();
        //$model->delete();
        return redirect(route('units.index'))->with('error_message', trans('cms.alert-remove', ['name' => $model->name]));
    }

}
