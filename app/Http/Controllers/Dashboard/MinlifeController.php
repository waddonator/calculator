<?php

namespace App\Http\Controllers\Dashboard;

use App\Minlife;
use App\Http\Requests\StoreMinlifeRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MinlifeController extends Controller
{
    public function index(Request $request)
    {
        $models = Minlife::where([['trash','=',false]])->orderby('year','DESC')->orderby('month','DESC')->paginate(20);
        return view('dashboard.minlife.index', [
                'models' => $models,
                'pagination_add' => $request->input(),
            ]);
    }

    public function create(Request $request)
    {
        $monthes = [];
        for ($i=1; $i < 13; $i++) { 
            $monthes[$i] = trans('calculator.month'.$i);
        }
        $years = [];
        for ($i=1999; $i < ((int)now()->format('Y'))+2; $i++) { 
            $years[$i] = $i;
        }
        return view('dashboard.minlife.create', [
                'monthes' => $monthes,
                'years' => $years,
            ]);
    }

    public function store(StoreMinlifeRequest $request)
    {
        $model = new Minlife;
        $model->month = $request->month;
        $model->year = $request->year;
        $model->value_baby = $request->value_baby;
        $model->value_child = $request->value_child;
        $model->value_worker = $request->value_worker;
        $model->value_invalid = $request->value_invalid;
        $model->value = $request->value;
        $model->save();
        return redirect()->route('minlifes.index')
            ->with('flash_message', trans('cms.alert-create', ['name' => $model->name]));
    }

    public function show($id)
    {
    }

    public function edit($id)
    {
        $monthes = [];
        for ($i=1; $i < 13; $i++) { 
            $monthes[$i] = trans('calculator.month'.$i);
        }
        $years = [];
        for ($i=1999; $i < ((int)now()->format('Y'))+2; $i++) { 
            $years[$i] = $i;
        }
        $model = Minlife::findOrFail($id);
        return view('dashboard.minlife.edit', [
                'model'=>$model,
                'monthes' => $monthes,
                'years' => $years,
            ]);
    }

    public function update(StoreMinlifeRequest $request, $id)
    {
        $model = Minlife::findOrFail($id);
        $model->month = $request->month;
        $model->year = $request->year;
        $model->value_baby = $request->value_baby;
        $model->value_child = $request->value_child;
        $model->value_worker = $request->value_worker;
        $model->value_invalid = $request->value_invalid;
        $model->value = $request->value;
        $model->save();
        return redirect()->route('minlifes.index')
            ->with('flash_message', trans('cms.alert-update', ['name' => $model->name]));
    }

    public function destroy(Request $request, $id)
    {
        $model = Minlife::findOrFail($id);
        $model->trash = true;
        $model->save();
        //$model->delete();
        return redirect(route('minlifes.index'))->with('error_message', trans('cms.alert-remove', ['name' => $model->name]));
    }

}
