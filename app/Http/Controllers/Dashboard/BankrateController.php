<?php

namespace App\Http\Controllers\Dashboard;

use App\Bankrate;
use App\Http\Requests\StoreBankrateRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BankrateController extends Controller
{
    public function index(Request $request)
    {
        $models = Bankrate::where([['trash','=',false]])->orderby('date','DESC')->paginate(20);
        return view('dashboard.bankrate.index', [
                'models' => $models,
                'pagination_add' => $request->input(),
            ]);
    }

    public function create(Request $request)
    {
        return view('dashboard.bankrate.create', [
            ]);
    }

    public function store(StoreBankrateRequest $request)
    {
        $model = new Bankrate;
        $model->date = $request->date;
        $model->value = $request->value;
        $model->save();
        return redirect()->route('bankrates.index')
            ->with('flash_message', trans('cms.alert-create', ['name' => $model->name]));
    }

    public function show($id)
    {
    }

    public function edit($id)
    {
        $model = Bankrate::findOrFail($id);
        return view('dashboard.bankrate.edit', [
                'model'=>$model,
            ]);
    }

    public function update(StoreBankrateRequest $request, $id)
    {
        $model = Bankrate::findOrFail($id);
        $model->date = $request->date;
        $model->value = $request->value;
        $model->save();
        return redirect()->route('bankrates.index')
            ->with('flash_message', trans('cms.alert-update', ['name' => $model->name]));
    }

    public function destroy(Request $request, $id)
    {
        $model = Bankrate::findOrFail($id);
        $model->trash = true;
        $model->save();
        //$model->delete();
        return redirect(route('bankrates.index'))->with('error_message', trans('cms.alert-remove', ['name' => $model->name]));
    }

}
