<?php

namespace App\Http\Controllers\Dashboard;

use App\Option;
use App\Http\Requests\StoreOptionRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OptionController extends Controller
{
    // public function __construct() {
    //     parent::__construct();
    //     $this->data['icon'] = 'icon-database-export';
    //     $this->data['title'] = trans('cms.export-data');
    //     array_push($this->data['breadcrumbs'] , ["title" => $this->data['title'],"url" => route('acts.index')]);
    // }

    public function index(Request $request)
    {
        $models = Option::where([['trash','=',false]])->orderby('id')->paginate(20);
        return view('dashboard.option.index', [
                'models' => $models,
                'pagination_add' => $request->input(),
            ]);
    }

    public function create(Request $request)
    {
        return view('dashboard.option.create', [
            ]);
    }

    public function store(StoreOptionRequest $request)
    {
        $model = new Option;
        $model->name = $request->name;
        $model->value = $request->value;
        $model->save();
        return redirect()->route('options.index')
            ->with('flash_message', trans('cms.alert-create', ['name' => $model->name]));
    }

    public function show($id)
    {
    }

    public function edit($id)
    {
        $model = Option::findOrFail($id);
        return view('dashboard.option.edit', [
                'model'=>$model,
            ]);
    }

    public function update(StoreOptionRequest $request, $id)
    {
        $model = Option::findOrFail($id);
        $model->name = $request->name;
        $model->value = $request->value;
        $model->save();
        return redirect()->route('options.index')
            ->with('flash_message', trans('cms.alert-update', ['name' => $model->name]));
    }

    public function destroy(Request $request, $id)
    {
        $model = Option::findOrFail($id);
        $model->trash = true;
        $model->save();
        //$model->delete();
        return redirect(route('options.index'))->with('error_message', trans('cms.alert-remove', ['name' => $model->name]));
    }

}
