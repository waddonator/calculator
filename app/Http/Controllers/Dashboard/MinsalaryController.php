<?php

namespace App\Http\Controllers\Dashboard;

use App\Minsalary;
use App\Http\Requests\StoreMinsalaryRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MinsalaryController extends Controller
{
    public function index(Request $request)
    {
        $models = Minsalary::where([['trash','=',false]])->orderby('year','DESC')->orderby('month','DESC')->paginate(20);
        return view('dashboard.minsalary.index', [
                'models' => $models,
                'pagination_add' => $request->input(),
            ]);
    }

    public function create(Request $request)
    {
        $monthes = [];
        for ($i=1; $i < 13; $i++) { 
            $monthes[$i] = trans('calculator.month'.$i);
        }
        $years = [];
        for ($i=1999; $i < ((int)now()->format('Y'))+2; $i++) { 
            $years[$i] = $i;
        }
        return view('dashboard.minsalary.create', [
                'monthes' => $monthes,
                'years' => $years,
            ]);
    }

    public function store(StoreMinsalaryRequest $request)
    {
        $model = new Minsalary;
        $model->month = $request->month;
        $model->year = $request->year;
        $model->value = $request->value;
        $model->save();
        return redirect()->route('minsalaries.index')
            ->with('flash_message', trans('cms.alert-create', ['name' => $model->name]));
    }

    public function show($id)
    {
    }

    public function edit($id)
    {
        $monthes = [];
        for ($i=1; $i < 13; $i++) { 
            $monthes[$i] = trans('calculator.month'.$i);
        }
        $years = [];
        for ($i=1999; $i < ((int)now()->format('Y'))+2; $i++) { 
            $years[$i] = $i;
        }
        $model = Minsalary::findOrFail($id);
        return view('dashboard.minsalary.edit', [
                'model'=>$model,
                'monthes' => $monthes,
                'years' => $years,
            ]);
    }

    public function update(StoreMinsalaryRequest $request, $id)
    {
        $model = Minsalary::findOrFail($id);
        $model->month = $request->month;
        $model->year = $request->year;
        $model->value = $request->value;
        $model->save();
        return redirect()->route('minsalaries.index')
            ->with('flash_message', trans('cms.alert-update', ['name' => $model->name]));
    }

    public function destroy(Request $request, $id)
    {
        $model = Minsalary::findOrFail($id);
        $model->trash = true;
        $model->save();
        //$model->delete();
        return redirect(route('minsalaries.index'))->with('error_message', trans('cms.alert-remove', ['name' => $model->name]));
    }

}
