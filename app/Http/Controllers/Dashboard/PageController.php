<?php

namespace App\Http\Controllers\Dashboard;

use App\Page;
use App\Http\Requests\StorePageRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
    public function index(Request $request)
    {
        $models = Page::where([['trash','=',false]])->orderby('id')->paginate(20);
        return view('dashboard.page.index', [
                'models' => $models,
                'pagination_add' => $request->input(),
            ]);
    }

    public function create(Request $request)
    {
        return view('dashboard.page.create', [
            ]);
    }

    public function store(StorePageRequest $request)
    {
        $model = new Page;
        $model->name = $request->name;
        $model->slug = $request->slug;
        $model->content = $request->content ? $request->content : '';
        $model->save();
        return redirect()->route('pages.index')
            ->with('flash_message', trans('cms.alert-create', ['name' => $model->name]));
    }

    public function show($id)
    {
    }

    public function edit($id)
    {
        $model = Page::findOrFail($id);
        return view('dashboard.page.edit', [
                'model'=>$model,
            ]);
    }

    public function update(StorePageRequest $request, $id)
    {
        $model = Page::findOrFail($id);
        $model->name = $request->name;
        $model->slug = $request->slug;
        $model->content = $request->content ? $request->content : '';
        $model->save();
        return redirect()->route('pages.index')
            ->with('flash_message', trans('cms.alert-update', ['name' => $model->name]));
    }

    public function destroy(Request $request, $id)
    {
        $model = Page::findOrFail($id);
        $model->trash = true;
        $model->save();
        //$model->delete();
        return redirect(route('pages.index'))->with('error_message', trans('cms.alert-remove', ['name' => $model->name]));
    }

}
