<?php

namespace App\Http\Middleware;

use Closure;

class Language
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $fallbackLocale = session('lang', config('app.fallback_locale'));

        // dd($fallbackLocale);

        if(!$request->is('dashboard*') && config('app.locales')) {
            $locale = $request->segment(1);

            if (!array_key_exists($locale, config('app.locales'))) {
                $segments = array_prepend($request->segments(), $fallbackLocale);
                return redirect(implode(DIRECTORY_SEPARATOR, $segments));
            }
            $fallbackLocale = $locale;
            session(['lang' => $locale]);
        }

        app()->setLocale($fallbackLocale);

        return $next($request);
    }
}
