<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    #
    # Обработчик калькуляторов
    # каждый обработчик содержится в отдельном файле с именем (slug).php,
    # с названием класса (Slug),в папке app\Calculate и функцией calculate
    #
    function calculate($data){
        $method = "App\\Calculate\\" . ucfirst($this->slug) ;
        if ( method_exists( ($method) , 'calculate')) {
            $controller = new $method;
            $result = $controller->calculate($data);
        } else {
            $result = 'Обробника калькулятора не знайдено';
        }
        return $result;
    }
}
