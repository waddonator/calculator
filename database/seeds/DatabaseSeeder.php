<?php

use App\Priceindex;
use App\Bankrate;
use App\Minsalary;
use App\Minlife;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $data = Storage::disk('db')->get('priceindexes.json');
        // Priceindex::insert(json_decode($data, true));
        // $data = Storage::disk('db')->get('bankrates.json');
        // Bankrate::insert(json_decode($data, true));
        // $data = Storage::disk('db')->get('minsalaries.json');
        // Minsalary::insert(json_decode($data, true));
        // $data = Storage::disk('db')->get('priceindexes2.json');
        // Priceindex::insert(json_decode($data, true));
        $data = Storage::disk('db')->get('minlifes.json');
        Minlife::insert(json_decode($data, true));

        // $this->call(UsersTableSeeder::class);
    }
}
