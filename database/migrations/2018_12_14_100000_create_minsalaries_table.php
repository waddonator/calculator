<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMinsalariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('minsalaries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('month')->unsigned()->default(0);
            $table->integer('year')->unsigned()->default(0);
            $table->float('value', 8, 2)->default(0);
            $table->boolean('trash')->default(false);
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('minsalaries');
    }
}
