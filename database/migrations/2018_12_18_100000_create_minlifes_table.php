<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMinlifesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('minlifes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('month')->unsigned()->default(0);
            $table->integer('year')->unsigned()->default(0);
            $table->float('value_baby', 8, 2)->default(0);
            $table->float('value_child', 8, 2)->default(0);
            $table->float('value_worker', 8, 2)->default(0);
            $table->float('value_invalid', 8, 2)->default(0);
            $table->float('value', 8, 2)->default(0);
            $table->boolean('trash')->default(false);
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('minlifies');
    }
}
