<?php

return [

    'home'      => 'Главная',
    'company'   => 'Компания',
    'products'  => 'Продукты и услуги',
    'news'      => 'Новости',

    'create'    => 'создание',
    'edit'      => 'изменение',

    'dashboard' => 'Панель управления',
    'users'     => 'Пользователи',
    'units'     => 'Калькуляторы',
    'pages'     => 'Страницы',
    'options'   => 'Параметры',
    'proposals' => 'Предложения',
];
