<a id="sended" href="#" data-toggle="modal" data-target="#sendedModal"></a>
<div class="modal fade" id="sendedModal" tabindex="-1" role="dialog" aria-labelledby="sendedModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="sendedModalLabel">Дякуємо</h6>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Всі ваші питанні та пропозиції будуть розглянуті найближчим часом. Про результати розгляду Вас буде проінформовано засобами електронного листування.</div>
            <div class="modal-footer">
                <button class="btn btn-light" type="button" data-dismiss="modal">Закрити</button>
            </div>
        </div>
    </div>
</div>
