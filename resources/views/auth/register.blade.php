<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" href="/assets/img/favicon.png" />
    <title>{{ config('app.name') }}</title>
    <link href="/assets/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/sb-admin.css" rel="stylesheet">
    <link href="/assets/css/common.css" rel="stylesheet">
</head>

<body class="bg-andro">
  <div class="container">
    <div class="card card-register mx-auto mt-5">
      <div class="card-header"><img src="/assets/img/favicon.png"> Register an Account</div>
      <div class="card-body">
        <form method="POST" action="{{ route('register') }}">
          @csrf
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <label for="name">First name</label>
                <input class="form-control" id="name" type="text" name="name" placeholder="Enter first name">
              </div>
            </div>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input class="form-control" id="email" type="email" name="email" placeholder="Enter email">
          </div>
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <label for="exampleInputPassword1">Password</label>
                <input class="form-control" id="password" name="password" type="password" placeholder="Password">
              </div>
              <div class="col-md-6">
                <label for="exampleConfirmPassword">Confirm password</label>
                <input class="form-control" id="confirmpassword" name="confirmpassword" type="password" placeholder="Confirm password">
              </div>
            </div>
          </div>
          <button type="submit" class="btn btn-primary btn-block">Register</button>
        </form>
        <div class="text-center">
          <a class="d-block small mt-3" href="{{route('login')}}">Login Page</a>
          <a class="d-block small" href="{{route('password.request')}}">Forgot Password?</a>
        </div>
      </div>
    </div>
  </div>
    <script src="/assets/lib/jquery/jquery.min.js"></script>
    <script src="/assets/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="/assets/lib/jquery-easing/jquery.easing.min.js"></script>
</body>

</html>
