@extends('layouts.layout')

@section('content')
    <h1 class="text-center mb-5">{{$model->name}}</h1>
    <article>{!!$model->content!!}</article>
@endsection

