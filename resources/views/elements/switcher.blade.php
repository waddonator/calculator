@if(config('app.locales'))
<form id='language'>
    <select name="lang" id="lang" class="form-control" onChange="get_action();">
        @foreach(config('app.locales') as $key => $locale)
            <option value="{{route('lang.switch',$key)}}" {{App::getLocale() == $key ? ' selected' : ''}}>{{$locale}}</option>
        @endforeach
    </select>
</form>
<script type="text/javascript">
    function get_action() {
        document.getElementById('language').action = document.getElementById('lang').value;
        document.getElementById('language').submit();
    }
</script>            
@endif