<div class="form-group row mb-5">
    <label for="monthstart" class="col-sm-4 col-form-label">Початок періоду</label>
    <div class="col-sm-8">
        <div class="input-group date">
            <input type="text" class="form-control" name="monthstart" id="monthstart" required>
            <div class="input-group-append input-group-addon">
                <span class="input-group-text"><i class="fa fa-fw fa-calendar"></i></span>
            </div>            
        </div>
    </div>
</div>

<div class="form-group row mb-5">
    <label for="monthfinish" class="col-sm-4 col-form-label">Кінець періоду</label>
    <div class="col-sm-8">
        <div class="input-group date">
            <input type="text" class="form-control" name="monthfinish" id="monthfinish" required>
            <div class="input-group-append input-group-addon">
                <span class="input-group-text"><i class="fa fa-fw fa-calendar"></i></span>
            </div>            
        </div>
    </div>
</div>

<div class="form-group row mb-5">
    <label for="monthbase" class="col-sm-4 col-form-label">Базовий місяць</label>
    <div class="col-sm-8">
        <div class="input-group date">
            <input type="text" class="form-control" name="monthbase" id="monthbase" required>
            <div class="input-group-append input-group-addon">
                <span class="input-group-text"><i class="fa fa-fw fa-calendar"></i></span>
            </div>            
        </div>
    </div>
</div>

<div class="form-group row mb-5">
    <label for="salary" class="col-sm-4 col-form-label">Сума нарахованої заробітної плати (грн)</label>
    <div class="col-sm-8">
        <input type="number" class="form-control" name="salary" id="salary" required>
    </div>
</div>

@section('scripts2')
<script>
    $(document).ready(function(){
        $('.input-group.date').datepicker({
            format: "mm.yyyy",
            //viewMode: "months",
            minViewMode: "months",
            maxViewMode: 0,
            language: "uk",
            autoclose: true
        });
    });
</script>
@endsection

