<h3 class="mb-5">Параметри розрахунку (п'ятиденний робочий тиждень)</h3>

<div class="form-group row mb-5">
    <label for="typevacation" class="col-sm-4 col-form-label">Вид відпустки</label>
    <div class="col-sm-8">
        <select class="form-control custom-select" name="typevacation" id="typevacation" required>
            <option value="1">Щорічна відпустка</option>
            <option value="2">Відпустка у зв’язку з навчанням</option>
            <option value="3">Відпустка без збереження заробітної плати</option>
        </select>
    </div>
</div>

{{--<div class="form-group row mb-5">
    <label for="datestart" class="col-sm-4 col-form-label">Дата початку відпустки</label>
    <div class="col-sm-8">
        <input type="date" class="form-control" name="datestart" id="datestart" required>
    </div>
</div>--}}

<div class="form-group row mb-5">
    <label for="datestart" class="col-sm-4 col-form-label">Базовий місяць</label>
    <div class="col-sm-8">
        <div class="input-group date">
            <input type="text" class="form-control" name="datestart" id="datestart" required>
            <div class="input-group-append input-group-addon">
                <span class="input-group-text"><i class="fa fa-fw fa-calendar"></i></span>
            </div>            
        </div>
    </div>
</div>

<div class="form-group row mb-5">
    <label for="daycount" class="col-sm-4 col-form-label">Кількість днів</label>
    <div class="col-sm-8">
        <input type="number" class="form-control" name="daycount" id="daycount" required>
    </div>
</div>

@section('scripts2')
<script>
    $(document).ready(function(){
        $('.input-group.date').datepicker({
            startDate: '-30y',
            endDate: '+30y',
            format: "dd.mm.yyyy",
            maxViewMode: 0,
            language: "uk",
            autoclose: true
        });
    });
</script>
@endsection

