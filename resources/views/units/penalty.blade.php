{{--<div class="form-group row mb-5">
    <label for="datestart" class="col-sm-4 col-form-label">Дата початку заборгованості</label>
    <div class="col-sm-8">
        <input type="date" class="form-control" name="datestart" id="datestart" required>
    </div>
</div>

<div class="form-group row mb-5">
    <label for="datestart" class="col-sm-4 col-form-label">Дата кінця заборгованості</label>
    <div class="col-sm-8">
        <input type="date" class="form-control" name="datefinish" id="datefinish" required>
    </div>
</div>--}}

<div class="form-group row mb-5">
    <label for="datestart" class="col-sm-4 col-form-label">Дата початку заборгованості</label>
    <div class="col-sm-8">
        <div class="input-group date">
            <input type="text" class="form-control" name="datestart" id="datestart" required>
            <div class="input-group-append input-group-addon">
                <span class="input-group-text"><i class="fa fa-fw fa-calendar"></i></span>
            </div>            
        </div>
    </div>
</div>

<div class="form-group row mb-5">
    <label for="datefinish" class="col-sm-4 col-form-label">Дата кінця заборгованості</label>
    <div class="col-sm-8">
        <div class="input-group date">
            <input type="text" class="form-control" name="datefinish" id="datefinish" required>
            <div class="input-group-append input-group-addon">
                <span class="input-group-text"><i class="fa fa-fw fa-calendar"></i></span>
            </div>            
        </div>
    </div>
</div>

<div class="form-group row mb-5">
    <label for="debt" class="col-sm-4 col-form-label">Сума заборгованості на момент її виникнення (грн)</label>
    <div class="col-sm-8">
        <input type="number" class="form-control" name="debt" id="debt" required>
    </div>
</div>

<div class="form-group row mb-5">
    <div class="custom-control custom-checkbox my-1 mr-sm-2">
        <input type="checkbox" class="custom-control-input form-control" id="check1" name="check1" checked>
        <label class="custom-control-label" for="check1">Підрахувати заборгованість з урахуванням індексу інфляції згідно зі ст. 625 ЦКУ</label>
    </div>
</div>

<div class="form-group row mb-5">
    <div class="custom-control custom-checkbox my-1 mr-sm-2">
        <input type="checkbox" class="custom-control-input form-control" id="check2"  name="check2">
        <label class="custom-control-label" for="check2">Підрахувати штрафні санкції</label>
    </div>
</div>

<div class="form-group row mb-5 show1"  style="display:none;">
    <label for="typevacation" class="col-sm-4 col-form-label">Вид штрафу (пені)</label>
    <div class="col-sm-8">
        <select class="form-control custom-select" name="typepenalty" id="typepenalty">
            <option value="1">{{trans('calculator.typepenalty1')}}</option>
            <option value="2">{{trans('calculator.typepenalty2')}}</option>
            <option value="3">{{trans('calculator.typepenalty3')}}</option>
            <option value="4">{{trans('calculator.typepenalty4')}}</option> 
        </select>
    </div>
</div>

<div class="form-group row mb-5 show2" style="display:none;">
    <label for="daycount" class="col-sm-4 col-form-label">Розмір штрафу (пені)</label>
    <div class="col-sm-8">
        <input type="number" class="form-control" name="size" id="size">
    </div>
</div>


@section('scripts2')
<script>
    $(document).ready(function(){
        $("#check2").change(function(){
            show1();
            show2()
        });
        $("#typepenalty").change(function(){
            show2();
        });

        function show1(){
            if ($("#check2").prop("checked")){
                $(".show1").show(100);
            } else {
                $(".show1").hide(100);
            }
        }

        function show2(){
            if (($("#typepenalty").val()==2 || $("#typepenalty").val()==4) && $("#check2").prop("checked")){
                $(".show2").show(100);
                $("#size").attr('required','required');
            } else {
                $(".show2").hide(100);
                $("#size").removeAttr('required');
            }
        }

        $('.input-group.date').datepicker({
            startDate: '-30y',
            endDate: '+30y',
            format: "dd.mm.yyyy",
            maxViewMode: 0,
            language: "uk",
            autoclose: true
        });
    });
</script>
@endsection
