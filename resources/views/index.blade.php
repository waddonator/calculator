@extends('layouts.layout')

@section('content')
    <div class="row mb-5">
        @foreach($models as $key => $model)
            @if($model->visible)
            <div class="col-lg-3 col-md-6 unit">
                <div class="unit-mt"></div>
                <a class="unit-content" data-hint="{{$model->hint}}" data-name="{{$model->name}}" href="{{route('units',$model->slug)}}"><img src="/storage/{{$model->image}}"><br>{{$model->name}}</a>
            </div>
            @else
            <div class="col-lg-3 col-md-6 unit">
                <div class="unit-mt"></div>
                <div class="unit-content-disabled" data-hint="{{$model->hint}}" data-name="{{$model->name}}"><img src="/storage/{{$model->image}}"><br>{{$model->name}}</div>
            </div>
            @endif
        @endforeach
    </div>
    <div class="hint" data-name="" style="border-top: 5px solid #7ac044;color: #465457;padding-top: 15px;font-size:14px;"></div>
@endsection

@section('scripts')
<script>
jQuery(document).ready(function(){
    $(".unit-content, .unit-content-disabled").hover(function(){
        if ($(".hint").attr('data-name') != $(this).attr('data-name')) {
            $(".hint").attr('data-name', $(this).attr('data-name'));
            $(".hint").hide().html('<h5>'+$(this).attr('data-name')+'</h5><p>'+$(this).attr('data-hint')+'</p>').fadeIn();
        }
    });
});</script>
@endsection


