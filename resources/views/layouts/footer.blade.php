<div class="container">
    <div class="footer-area bg-blue">
        <div class="row">
            <div class="col-lg-6">
                <h2 class="mb-5">Контакти</h2>
                <ul class="contacts mb-5">
                    <li class="mb-3 @getoption('showphone')">
                        <div class="rounded-circle p-2 bg-light-blue contact-thumb"><i class="fa fa-fw fa-2x fa-phone"></i></div>
                        <div>@getoption('phone')</div>
                    </li>
                    <li class="mb-3 @getoption('showemail')">
                        <div class="rounded-circle p-2 bg-light-blue contact-thumb"><i class="fa fa-fw fa-2x fa-envelope"></i></div>
                        <div>@getoption('email')</div>
                    </li>
                    <li class="@getoption('showadress')">
                        <div class="rounded-circle p-2 bg-light-blue contact-thumb"><i class="fa fa-fw fa-2x fa-map-marker"></i></div>
                        <div>@getoption('adress')</div>
                    </li>
                </ul>
                <h2 class="mb-5">Ми в соц. мережах</h2>
                <ul class="social">
                    <li><a href="@getoption('facebook')"><i class="fa fa-2x fa-facebook mr-2"></i></a></li>
                    <li><a href="@getoption('twitter')"><i class="fa fa-2x fa-twitter mr-2"></i></a></li>
                    <li><a href="@getoption('linkedin')"><i class="fa fa-2x fa-linkedin mr-2"></i></a></li>
                </ul>
            </div>
            <div class="col-lg-6">
                <h3 class="mt-5 mb-5">Є питання або пропозиції? Напишіть нам</h3>
                <form id="send-form">
                    <div class="row">
                        <div class="col-md-6">
                            <input name="name" id="name" type="text" class="my-form-control" placeholder="Ім'я" required>
                        </div>
                        <div class="col-md-6">
                            <input name="email" id="email" type="email" class="my-form-control" placeholder="Email" required>
                        </div>
                        <div class="col-md-12 mt-3">
                            <textarea name="content" id="content" cols="30" rows="5" class="my-form-control" placeholder="Повідомлення" required></textarea>
                        </div>
                    </div>
                    <div class="text-right mt-3"><button class="btn btn-red">Надіслати <i class="fa fa-long-arrow-right"></i></button></div>
                </form>
            </div>
        </div>
    </div>
    <div class="bottom-line">© Activelex 2018</div>
</div>

<div id = "GotoTop" ><i class="fa fa-fw fa-angle-up fa-2x" aria-hidden="true"></i></div>

@include('modals.sended')

    <!-- Bootstrap core JavaScript-->
    <script src="/assets/lib/jquery/jquery.min.js"></script>
    <script src="/assets/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="/assets/lib/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="/assets/lib/bootstrap-datepicker/locales/bootstrap-datepicker.uk.min.js" charset="UTF-8"></script>
    <script src="/assets/lib/printme/jquery-printme.js"></script>
    <script src="/js/common.js"></script>
<script>
$(document).ready(function(){
    $("#send-form").submit(function(e){
        e.preventDefault();
        $('#name').val('');
        $('#email').val('');
        $('#content').val('');
        $.ajax({
            url: '{{route('proposal')}}',
            type: 'POST',
            dataType : "json",
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            beforeSend: function() {
            },
            success: function (data) {
                console.log(data);
                $("#sended").click();
            },
            error: function(){
                console.log('ajax-error');
            }
        });

        //console.log($('meta[name="csrf-token"]').attr('content'));
    });
});
</script>

    @section('scripts')
    @show
    @section('scripts2')
    @show

    </div>
</body>

</html>
