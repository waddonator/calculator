@include('layouts.header')

    <div class="container">
        <div class="content-area bg-gray">
            @section('content')
            @show
        </div>
    </div>

@include('layouts.footer')