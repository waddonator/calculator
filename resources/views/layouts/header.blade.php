<!DOCTYPE html>
<html lang="{{app()->getLocale()}}">

<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" href="/assets/img/favicon.png" />
    <title>{{ config('app.name') }}</title>
    <link href="https://fonts.googleapis.com/css?family=Fira+Sans:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;subset=cyrillic" rel="stylesheet">
    <link href="/assets/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link id="bsdp-css" href="/assets/lib/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">
    <link href="/assets/lib/bootstrap/css/bootstrap-hexa.css" rel="stylesheet">
    <link href="/assets/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="/css/common.css" rel="stylesheet" type="text/css">
</head>

<body>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-112604254-3"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-112604254-3');
</script>
    <div class="container">
        <div class="topline">
            <div class="img-wrap"><a href="{{route('home')}}"><img src="/assets/img/activeLex_logo_calculator(ukr).png" alt=""></a></div>
            @include('elements.switcher')
            <ul class="social">
                <li><a href="@getoption('facebook')"><i class="fa fa-2x fa-facebook mr-2"></i></a></li>
                <li><a href="@getoption('twitter')"><i class="fa fa-2x fa-twitter mr-2"></i></a></li>
                <li><a href="@getoption('linkedin')"><i class="fa fa-2x fa-linkedin mr-2"></i></a></li>
            </ul>
        </div>
    </div>
    <div class="menu text-center bg-blue">
        <div class="container">
            <button class="mobil-button"><i class="fa fa-reorder"></i></button>
            <ul class="inner-menu">
                <li><a href="{{route('home')}}">{{trans('calculator.home')}}</a></li>
                <li><a href="{{route('page','about')}}">{{trans('calculator.company')}}</a></li>
                {{--<li><a href="{{route('page','products')}}">{{trans('calculator.products')}}</a></li>--}}
                <li><a href="http://start.activelex.com/">{{trans('calculator.products')}}</a></li>
                {{--<li><a href="{{route('page','news')}}">{{trans('calculator.news')}}</a></li>--}}
                <li><a href="https://lexinform.com.ua/">{{trans('calculator.news')}}</a></li>
            </ul>
        </div>
    </div>