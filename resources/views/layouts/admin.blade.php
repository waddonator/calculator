<!DOCTYPE html>
<html lang="{{app()->getLocale()}}">

<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" href="/assets/img/favicon.png" />
    <title>{{ config('app.name') }}</title>
    <link href="https://fonts.googleapis.com/css?family=Fira+Sans:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;subset=cyrillic" rel="stylesheet">
    <link href="/assets/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/lib/datatables/datatables.min.css" rel="stylesheet" type="text/css"/>    
    <link href="/css/common.css" rel="stylesheet" type="text/css">
    <link href="/css/admin.css" rel="stylesheet" type="text/css">
</head>

<body>

    <div class="bg-blue p-3 d-flex align-items-center justify-content-between">
        <div class="img-wrap"><a href="{{route('home')}}"><img src="/assets/img/activeLex_logo_calculator(ukr).png" alt=""></a></div>
        <div>
            <a href="#" class="text-white" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-2x fa-fw fa-long-arrow-right"></i></a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
        </div>
    </div>

    <div class="content-wrapper d-flex">
        <aside>
            <ul>
                <li><a href="{{route('dashboard')}}"><i class="fa fa-fw fa-dashboard"></i> {{trans('calculator.dashboard')}}</a></li>
                <li><a href="{{route('users.index')}}"><i class="fa fa-fw fa-user"></i> {{trans('calculator.users')}}</a></li>
                <li><a href="{{route('units.index')}}"><i class="fa fa-fw fa-desktop"></i> {{trans('calculator.units')}}</a></li>
                <li><a href="{{route('pages.index')}}"><i class="fa fa-fw fa-file"></i> {{trans('calculator.pages')}}</a></li>
                <li><a href="{{route('options.index')}}"><i class="fa fa-fw fa-cog"></i> {{trans('calculator.options')}}</a></li>
                <li><a href="#"><i class="fa fa-fw fa-envelope"></i> {{trans('calculator.proposals')}}</a></li>
                <hr>
                <li><a href="{{route('holidays.index')}}"><i class="fa fa-fw fa-calendar"></i> {{trans('calculator.holidays')}}</a></li>
                <li><a href="{{route('priceindexes.index')}}"><i class="fa fa-fw fa-shopping-cart"></i> {{trans('calculator.priceindexes')}}</a></li>
                <li><a href="{{route('bankrates.index')}}"><i class="fa fa-fw fa-university"></i> {{trans('calculator.bankrates')}}</a></li>
                <li><a href="{{route('minsalaries.index')}}"><i class="fa fa-fw fa-credit-card"></i> {{trans('calculator.minsalaries')}}</a></li>
                <li><a href="{{route('minlifes.index')}}"><i class="fa fa-fw fa-cutlery"></i> {{trans('calculator.minlifes')}}</a></li>
            </ul>
        </aside>
        <div class="p-5 content-area">
            <!-- Alerts -->
            @if(Session::has('flash_message'))
                <div class="alert alert-success alert-styled-left alert-arrow-left alert-dismissible mt-3">
                    <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
                    {!! session('flash_message') !!}
                </div>                    
            @endif
            @if(Session::has('error_message'))
                <div class="alert alert-warning alert-styled-left alert-arrow-left alert-dismissible mt-3">
                    <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
                    {!! session('error_message') !!}
                </div>
            @endif
            <!-- /alerts -->
            @section('content')
            @show
        </div>
    </div>
    <footer class="text-center">
        Admin area of calculators
    </footer>

    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">Выход</h6>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <a class="btn btn-primary" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Выход</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
                    <button class="btn btn-light" type="button" data-dismiss="modal">Отмена</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Remove Modal-->
    <div id="removeModal" class="modal fade" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header bg-danger text-white">
                                <h6 class="modal-title">Удаление элемента</h6>
                                <button type="button" class="close text-white" data-dismiss="modal">&times;</button>
                            </div>

                            <div class="modal-body" id="remove-modal-body">
                                Вы уверены, что хотите удалить элемент без возможности восстановления?
                            </div>
                            <form id="remove-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf
                            <input name="_method" type="hidden" value="DELETE"><input name="_action" type="hidden" value="DELETE"></form>
                            <div class="modal-footer">
                                <button type="button" class="btn bg-danger text-white" onclick="document.getElementById('remove-form').submit()">Удалить</button>
                                <button type="button" class="btn btn-light" data-dismiss="modal">Отмена</button>
                            </div>
                        </div>
                    </div>
                </div>

    @section('modals')
    @show

    <!-- Bootstrap core JavaScript-->
    <script src="/assets/lib/jquery/jquery.min.js"></script>
    <script src="/assets/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="/assets/lib/bootbox/bootbox.min.js"></script>
    <script type="text/javascript" src="/assets/lib/datatables/datatables.min.js"></script>
    <script type="text/javascript" src="/assets/lib/tinymce/tinymce.min.js"></script>
    <script>tinymce.init({selector:'.tinymce',height : "400",plugins: "code",});</script>
    <script src="/js/common.js"></script>

    @section('scripts')
    @show

</body>

</html>
