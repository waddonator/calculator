@extends('layouts.layout')

@section('content')
    <h1 class="text-center mb-5" id="title">{{$model->name}}</h1>

    <div class="wrapper">
        <div class="tabs text-center">
            <span class="tab active">Форма заповнення</span>
            <span class="tab">Результати обчислення</span>
        </div>
        <div class="tab-content">
            <form id="calculate-form">
            <div class="tab-item">
                <div class="tab-body">
                    @if(View::exists('units.' . $model->slug ))
                        @include('units.' . $model->slug )
                    @else
                        Представлення для обраного калькулятора відсутнє
                    @endif
                </div>
                <div class="tab-footer d-flex">
                    <div class="space"></div>
                    <div>Розрахувати&nbsp;&nbsp;</div>
                    <button id="calculate" class="bg-danger"><img src="/assets/img/forward.png" alt=""></button>
                </div>
            </div>
            </form>
            <div class="tab-item">
                <div class="tab-body" id="answer">
                </div>
                <div class="tab-footer d-flex">
                    <button id="back" class="bg-primary"><img src="/assets/img/back.png" alt=""></button>
                    <div>&nbsp;&nbsp;Повернутися до розрахунків</div>
                    <div class="space"></div>
                    <div>Роздрукувати&nbsp;&nbsp;</div>
                    <button id="print" class="bg-danger"><img src="/assets/img/print.png" alt=""></button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script>
jQuery(document).ready(function(){
    var free = true;
    $(".tab-item").not(":first").hide();
    $(".wrapper .tab").click(function() {
	    $(".wrapper .tab").removeClass("active").eq($(this).index()).addClass("active");
	    $(".tab-item").hide().eq($(this).index()).fadeIn()
    }).eq(0).addClass("active");

    $("#calculate-form").submit(function(e){
        e.preventDefault();
        var params = {};
        $("#calculate-form .form-control").each(function(){
            if ($( this ).attr('type') != 'checkbox'){
                params[$( this ).attr('name')] = $( this ).val();
            } else {
                params[$( this ).attr('name')] = $( this ).prop("checked");
            }
        });
        console.log( params );
        if (free){
            $.ajax({
                url: '{{route('calculate' , $model->slug)}}',
                type: 'POST',
                dataType : "json",
                data: { data : JSON.stringify(params) },
                headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                beforeSend: function() {
                    free = false;
                },
                success: function (data) {
                    $(".wrapper .tab").removeClass("active").eq(1).addClass("active");
                    $(".tab-item").hide().eq(1).fadeIn();
                    $('body,html').animate({scrollTop:$('#title').offset().top},800);
                    $('#answer').html(data);
                    free = true;
                },
                error: function(jqXHR, exception){
                    $(".wrapper .tab").removeClass("active").eq(1).addClass("active");
                    $(".tab-item").hide().eq(1).fadeIn();
                    $('body,html').animate({scrollTop:$('#title').offset().top},800);
                    $('#answer').html('Помилка ' + jqXHR.status + '. Спробуйте перезавантажити сторінку.');
                    free = true;
                }
            });
        }
    });

    $("#back").click(function(){
        $(".wrapper .tab").removeClass("active").eq(0).addClass("active");
                    $(".tab-item").hide().eq(0).fadeIn();
    });

    $("#print").click(function(){
        $('#answer').printMe();
    });

});</script>
@endsection
