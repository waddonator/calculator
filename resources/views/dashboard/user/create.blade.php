@extends('layouts.admin')

@section('content')

{{ Form::open(array('url' => 'dashboard/users')) }}
    <h1 class="mb-5">{{trans('calculator.users')}} - {{trans('calculator.create')}}</h1>
    <div class="form-group {{ $errors->has('name') ? 'has-danger' : '' }}">
        {{ Form::label('name', trans('table.username'), ['class'=>'small text-muted font-italic']) }}
        {{ Form::text('name', null, array('class' => 'form-control'. ($errors->has('name') ? ' is-invalid' : '' ))) }}
        @if($errors->has('name'))
            @foreach ($errors->get('name') as $message)
                <div class="invalid-feedback">{{ $message }}</div>
            @endforeach
        @endif
    </div>

    <div class="form-group {{ $errors->has('email') ? 'has-danger' : '' }}">
        {{ Form::label('email', trans('table.email'), ['class'=>'small text-muted font-italic']) }}
        {{ Form::email('email', null, array('class' => 'form-control'. ($errors->has('email') ? ' is-invalid' : '' ))) }}
        @if($errors->has('email'))
            @foreach ($errors->get('email') as $message)
                <div class="invalid-feedback">{{ $message }}</div>
            @endforeach
        @endif
    </div>

    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
        {{ Form::label('password', trans('table.password'), ['class'=>'small text-muted font-italic']) }}<br>
        {{ Form::password('password', array('class' => 'form-control'. ($errors->has('password') ? ' is-invalid' : '' ))) }}
        @if($errors->has('password'))
            @foreach ($errors->get('password') as $message)
                <div class="invalid-feedback">{{ $message }}</div>
            @endforeach
        @endif
    </div>

    <div class="form-group">
        {{ Form::label('password_confirmation', trans('table.password-confirmation'), ['class'=>'small text-muted font-italic']) }}<br>
        {{ Form::password('password_confirmation', array('class' => 'form-control'. ($errors->has('password') ? ' is-invalid' : '' ))) }}
        @if($errors->has('password_confirmation'))
            @foreach ($errors->get('password_confirmation') as $message)
                <div class="invalid-feedback">{{ $message }}</div>
            @endforeach
        @endif
    </div>
    <button type="submit" id="submit" class="btn btn-success mt-3" title="Save"><i class="fa fa-save fa-fw"></i></button>

{{ Form::close() }}
@endsection
