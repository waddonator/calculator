@extends('layouts.admin')

@section('content')
    <h1 class="mb-5">{{trans('calculator.minlifes')}}</h1>
    <table class="table table-striped w-100">
        <thead>
            <tr>
                <th class="align-middle">Код</th>
                <th class="align-middle">Місяць</th>
                <th class="align-middle">Рік</th>
                <th class="align-middle">Діти віком до 6 років</th>
                <th class="align-middle">Діти віком від 6 до 18 років</th>
                <th class="align-middle">Працездатні особи</th>
                <th class="align-middle">Особи, які втратили працездатність</th>
                <th class="align-middle">Загальний показник</th>
                <th class="align-middle">Дія</th>
            </tr>
        </thead>
        <tbody>
            @foreach($models as $key => $model)
            <tr>
                <td class="text-center">{{$model->id}}</td>
                <td>{{trans('calculator.month'.$model->month)}}</td>
                <td class="text-center">{{$model->year}}</td>
                <td class="text-right">{{$model->value_baby}}</td>
                <td class="text-right">{{$model->value_child}}</td>
                <td class="text-right">{{$model->value_worker}}</td>
                <td class="text-right">{{$model->value_invalid}}</td>
                <td class="text-right">{{$model->value}}</td>
                <td class="text-center">
                    <div class="btn-group">
                        <a href="{{route('minlifes.edit',$model->id)}}" class="btn btn-info btn-sm" title="Змінити"><i class="fa fa-pencil fa-fw"></i></a>
                        <a href="{{route('minlifes.destroy',$model->id)}}" class="btn btn-danger btn-sm btn-remove" data-toggle="modal" data-target="#removeModal" title="Видалити"><i class="fa fa-trash-o fa-fw"></i></a>
                   </div>                    
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

    <div class="d-flex mt-3 justify-content-between">
        <a href="{{ route('minlifes.create') }}" class="btn btn-success" title="{{trans('cms.btn-append')}}"><i class="fa fa-fw fa-plus"></i></a>
        {{ $models->appends($pagination_add)->links() }}
    </div>

@endsection


