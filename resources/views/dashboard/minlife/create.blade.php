@extends('layouts.admin')

@section('content')

{{ Form::open(array('url' => 'dashboard/minlifes')) }}
    <h1 class="mb-5">{{trans('calculator.minlifes')}} - {{trans('calculator.create')}}</h1>
    <div class="form-group">
        {{ Form::label('month', 'Месяц', ['class'=>'small text-muted font-italic']) }}
        {{ Form::select('month', $monthes, null, array('class' => 'form-control'. ($errors->has('month') ? ' is-invalid' : ''))) }}
        @if($errors->has('month'))
            @foreach ($errors->get('month') as $message)
                <div class="invalid-feedback">{{ $message }}</div>
            @endforeach
        @endif
    </div>
    <div class="form-group">
        {{ Form::label('year', 'Год', ['class'=>'small text-muted font-italic']) }}
        {{ Form::select('year', $years, null, array('class' => 'form-control'. ($errors->has('year') ? ' is-invalid' : ''))) }}
        @if($errors->has('year'))
            @foreach ($errors->get('year') as $message)
                <div class="invalid-feedback">{{ $message }}</div>
            @endforeach
        @endif
    </div>
    <div class="form-group">
        {{ Form::label('value_baby', 'Значение baby', ['class'=>'small text-muted font-italic']) }}
        {{ Form::number('value_baby', null, array('step'=>'0.01', 'class' => 'form-control'. ($errors->has('value_baby') ? ' is-invalid' : '' ))) }}
        @if($errors->has('value_baby'))
            @foreach ($errors->get('value_baby') as $message)
                <div class="invalid-feedback">{{ $message }}</div>
            @endforeach
        @endif
    </div>
    <div class="form-group">
        {{ Form::label('value_child', 'Значение child', ['class'=>'small text-muted font-italic']) }}
        {{ Form::number('value_child', null, array('step'=>'0.01', 'class' => 'form-control'. ($errors->has('value_child') ? ' is-invalid' : '' ))) }}
        @if($errors->has('value_child'))
            @foreach ($errors->get('value_child') as $message)
                <div class="invalid-feedback">{{ $message }}</div>
            @endforeach
        @endif
    </div>
    <div class="form-group">
        {{ Form::label('value_worker', 'Значение worker', ['class'=>'small text-muted font-italic']) }}
        {{ Form::number('value_worker', null, array('step'=>'0.01', 'class' => 'form-control'. ($errors->has('value_worker') ? ' is-invalid' : '' ))) }}
        @if($errors->has('value_worker'))
            @foreach ($errors->get('value_worker') as $message)
                <div class="invalid-feedback">{{ $message }}</div>
            @endforeach
        @endif
    </div>
    <div class="form-group">
        {{ Form::label('value_invalid', 'Значение invalid', ['class'=>'small text-muted font-italic']) }}
        {{ Form::number('vavalue_invalidlue', null, array('step'=>'0.01', 'class' => 'form-control'. ($errors->has('value_invalid') ? ' is-invalid' : '' ))) }}
        @if($errors->has('value_invalid'))
            @foreach ($errors->get('value_invalid') as $message)
                <div class="invalid-feedback">{{ $message }}</div>
            @endforeach
        @endif
    </div>
    <div class="form-group">
        {{ Form::label('value', 'Значение', ['class'=>'small text-muted font-italic']) }}
        {{ Form::number('value', null, array('step'=>'0.01', 'class' => 'form-control'. ($errors->has('value') ? ' is-invalid' : '' ))) }}
        @if($errors->has('value'))
            @foreach ($errors->get('value') as $message)
                <div class="invalid-feedback">{{ $message }}</div>
            @endforeach
        @endif
    </div>
    <button type="submit" id="submit" class="btn btn-success mt-3" title="Save"><i class="fa fa-save fa-fw"></i></button>

{{ Form::close() }}

@endsection
