@extends('layouts.admin')

@section('content')

{{ Form::open(array('url' => 'dashboard/holidays')) }}
    <h1 class="mb-5">{{trans('calculator.holidays')}} - {{trans('calculator.create')}}</h1>
    <div class="form-group">
        {{ Form::label('name', 'Название', ['class'=>'small text-muted font-italic']) }}
        {{ Form::text('name', null, array('class' => 'form-control'. ($errors->has('name') ? ' is-invalid' : '' ))) }}
        @if($errors->has('name'))
            @foreach ($errors->get('name') as $message)
                <div class="invalid-feedback">{{ $message }}</div>
            @endforeach
        @endif
    </div>
    <div class="form-group">
        {{ Form::label('date', 'Дата', ['class'=>'small text-muted font-italic']) }}
        {{ Form::date('date', null, array('class' => 'form-control'. ($errors->has('date') ? ' is-invalid' : '' ))) }}
        @if($errors->has('date'))
            @foreach ($errors->get('date') as $message)
                <div class="invalid-feedback">{{ $message }}</div>
            @endforeach
        @endif
    </div>
    <div class="form-group">
        {{ Form::label('type', 'Тип', ['class'=>'small text-muted font-italic']) }}
        {{ Form::select('type', $types, null, array('class' => 'form-control'. ($errors->has('type') ? ' is-invalid' : ''))) }}
        @if($errors->has('type'))
            @foreach ($errors->get('type') as $message)
                <div class="invalid-feedback">{{ $message }}</div>
            @endforeach
        @endif
    </div>
    <div class="form-group">
        {{ Form::label('transfer', 'Дата отработки', ['class'=>'small text-muted font-italic']) }}
        {{ Form::date('transfer', null, array('class' => 'form-control'. ($errors->has('transfer') ? ' is-invalid' : '' ))) }}
        @if($errors->has('transfer'))
            @foreach ($errors->get('transfer') as $message)
                <div class="invalid-feedback">{{ $message }}</div>
            @endforeach
        @endif
    </div>
    <button type="submit" id="submit" class="btn btn-success mt-3" title="Save"><i class="fa fa-save fa-fw"></i></button>

{{ Form::close() }}

@endsection
