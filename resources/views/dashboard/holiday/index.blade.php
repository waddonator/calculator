@extends('layouts.admin')

@section('content')
    <h1 class="mb-5">{{trans('calculator.holidays')}}</h1>
    <table class="table table-striped w-100">
        <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Date</th>
                <th>Type</th>
                <th>Transfer</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach($models as $key => $model)
            <tr>
                <td class="text-center">{{$model->id}}</td>
                <td>{{$model->name}}</td>
                <td class="text-center">@if($model->date){{$model->date->format('Y-m-d')}}@endif</td>
                <td class="text-center">{{trans('calculator.holidaytype'.$model->type)}}</td>
                <td class="text-center">@if($model->transfer){{$model->transfer->format('Y-m-d')}}@endif</td>
                <td class="text-center">
                    <div class="btn-group">
                        <a href="{{route('holidays.edit',$model->id)}}" class="btn btn-info btn-sm" title="Змінити"><i class="fa fa-pencil fa-fw"></i></a>
                        <a href="{{route('holidays.destroy',$model->id)}}" class="btn btn-danger btn-sm btn-remove" data-toggle="modal" data-target="#removeModal" title="Видалити"><i class="fa fa-trash-o fa-fw"></i></a>
                   </div>                    
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

    <div class="d-flex mt-3 justify-content-between">
        <a href="{{ route('holidays.create') }}" class="btn btn-success" title="{{trans('cms.btn-append')}}"><i class="fa fa-fw fa-plus"></i></a>
        {{ $models->appends($pagination_add)->links() }}
    </div>

@endsection


