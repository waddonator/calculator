@extends('layouts.admin')

@section('content')

{{ Form::open(array('url' => 'dashboard/options')) }}
    <h1 class="mb-5">{{trans('calculator.options')}} - {{trans('calculator.create')}}</h1>
    <div class="form-group">
        {{ Form::label('name', 'Параметр', ['class'=>'small text-muted font-italic']) }}
        {{ Form::text('name', null, array('class' => 'form-control'. ($errors->has('name') ? ' is-invalid' : '' ))) }}
        @if($errors->has('name'))
            @foreach ($errors->get('name') as $message)
                <div class="invalid-feedback">{{ $message }}</div>
            @endforeach
        @endif
    </div>
    <div class="form-group">
        {{ Form::label('value', 'Значение', ['class'=>'small text-muted font-italic']) }}
        {{ Form::text('value', null, array('class' => 'form-control'. ($errors->has('value') ? ' is-invalid' : '' ))) }}
        @if($errors->has('value'))
            @foreach ($errors->get('value') as $message)
                <div class="invalid-feedback">{{ $message }}</div>
            @endforeach
        @endif
    </div>
    <button type="submit" id="submit" class="btn btn-success mt-3" title="Save"><i class="fa fa-save fa-fw"></i></button>

{{ Form::close() }}

@endsection
