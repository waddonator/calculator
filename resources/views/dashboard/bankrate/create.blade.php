@extends('layouts.admin')

@section('content')

{{ Form::open(array('url' => 'dashboard/bankrates')) }}
    <h1 class="mb-5">{{trans('calculator.bankrates')}} - {{trans('calculator.create')}}</h1>
    <div class="form-group">
        {{ Form::label('date', 'Дата', ['class'=>'small text-muted font-italic']) }}
        {{ Form::date('date', null, array('class' => 'form-control'. ($errors->has('date') ? ' is-invalid' : '' ))) }}
        @if($errors->has('date'))
            @foreach ($errors->get('date') as $message)
                <div class="invalid-feedback">{{ $message }}</div>
            @endforeach
        @endif
    </div>
    <div class="form-group">
        {{ Form::label('value', 'Значение', ['class'=>'small text-muted font-italic']) }}
        {{ Form::number('value', null, array('step'=>'0.01', 'class' => 'form-control'. ($errors->has('value') ? ' is-invalid' : '' ))) }}
        @if($errors->has('value'))
            @foreach ($errors->get('value') as $message)
                <div class="invalid-feedback">{{ $message }}</div>
            @endforeach
        @endif
    </div>
    <button type="submit" id="submit" class="btn btn-success mt-3" title="Save"><i class="fa fa-save fa-fw"></i></button>

{{ Form::close() }}

@endsection
