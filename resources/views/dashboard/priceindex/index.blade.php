@extends('layouts.admin')

@section('content')
    <h1 class="mb-5">{{trans('calculator.priceindexes')}}</h1>
    <table class="table table-striped w-100" id="datatable">
        <thead>
            <tr>
                <th>Id</th>
                <th>Month</th>
                <th>Year</th>
                <th>Value</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Id</th>
                <th>Month</th>
                <th>Year</th>
                <th>Value</th>
                <th>Actions</th>
            </tr>
        </tfoot>
    </table>

    {{--<table class="table table-striped w-100">
        <thead>
            <tr>
                <th>Id</th>
                <th>Month</th>
                <th>Year</th>
                <th>Value</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach($models as $key => $model)
            <tr>
                <td class="text-center">{{$model->id}}</td>
                <td>{{trans('calculator.month'.$model->month)}}</td>
                <td class="text-center">{{$model->year}}</td>
                <td class="text-right">{{$model->value}}</td>
                <td class="text-center">
                    <div class="btn-group">
                        <a href="{{route('priceindexes.edit',$model->id)}}" class="btn btn-info btn-sm" title="Змінити"><i class="fa fa-pencil fa-fw"></i></a>
                        <a href="{{route('priceindexes.destroy',$model->id)}}" class="btn btn-danger btn-sm btn-remove" data-toggle="modal" data-target="#removeModal" title="Видалити"><i class="fa fa-trash-o fa-fw"></i></a>
                   </div>                    
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>--}}

    <div class="d-flex mt-3 justify-content-between">
        <a href="{{ route('priceindexes.create') }}" class="btn btn-success" title="{{trans('cms.btn-append')}}"><i class="fa fa-fw fa-plus"></i></a>
        {{-- $models->appends($pagination_add)->links() --}}
    </div>

@endsection

@section('scripts')
<script>
    var table = $("#datatable").DataTable({
        "autoWidth" : true,
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "pageLength": 25,
        "ajax": {
            "url": "{{route('getpriceindexes')}}",
            "dataType": "json",
            "type": "POST",
            "data": {"_token":"{{ csrf_token() }}" },
        },
        "columns":[
            {"data":"id"},
            {"data":"month"},
            {"data":"year"},
            {"data":"value"},
            {"data":"actions","searchable":false,"orderable":false}
        ],
        'columnDefs': [
            {
                "targets": 0,
                "className": "text-center",
            },
            {
                "targets": 4,
                "className": "text-center",
            },
        ],
        "language": {
            "url": "/assets/lib/datatables/languages/ukrainian.json"
        },
        "buttons": [
            'copy', 'excel', 'pdf'
        ]        
    });
</script>
@endsection