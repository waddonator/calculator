@extends('layouts.admin')

@section('content')

{{ Form::model($model, array('route' => array('pages.update', $model->id), 'method' => 'PUT')) }}
    <h1 class="mb-5">{{trans('calculator.pages')}} - {{trans('calculator.edit')}} (<span class="font-weight-bold">{{$model->name}}</span>)</h1>
    <div class="form-group">
        {{ Form::label('name', 'Параметр', ['class'=>'small text-muted font-italic']) }}
        {{ Form::text('name', null, array('class' => 'form-control'. ($errors->has('name') ? ' is-invalid' : '' ))) }}
        @if($errors->has('name'))
            @foreach ($errors->get('name') as $message)
                <div class="invalid-feedback">{{ $message }}</div>
            @endforeach
        @endif
    </div>
    <div class="form-group">
        {{ Form::label('slug', 'Слаг', ['class'=>'small text-muted font-italic']) }}
        {{ Form::text('slug', null, array('class' => 'form-control'. ($errors->has('slug') ? ' is-invalid' : '' ))) }}
        @if($errors->has('slug'))
            @foreach ($errors->get('slug') as $message)
                <div class="invalid-feedback">{{ $message }}</div>
            @endforeach
        @endif
    </div>
    <div class="form-group">
        {{ Form::label('content', trans('cms.column-content'), ['class'=>'small text-muted font-italic']) }}
        {{ Form::textarea('content', null, ['class' => 'form-control tinymce' . ($errors->has('content') ? ' is-invalid' : '')]) }}
        @if($errors->has('content'))
            @foreach ($errors->get('content') as $message)
                <div class="invalid-feedback">{{ $message }}</div>
            @endforeach
        @endif
    </div>
    <button type="submit" id="submit" class="btn btn-success mt-3" title="Save"><i class="fa fa-save fa-fw"></i></button>

{{ Form::close() }}

@endsection
