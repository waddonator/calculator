@extends('layouts.admin')

@section('content')

{{ Form::model($model, array('route' => array('minsalaries.update', $model->id), 'method' => 'PUT')) }}
    <h1 class="mb-5">{{trans('calculator.minsalaries')}} - {{trans('calculator.edit')}} (<span class="font-weight-bold">{{$model->name}}</span>)</h1>
    <div class="form-group">
        {{ Form::label('month', 'Месяц', ['class'=>'small text-muted font-italic']) }}
        {{ Form::select('month', $monthes, null, array('class' => 'form-control'. ($errors->has('month') ? ' is-invalid' : ''))) }}
        @if($errors->has('month'))
            @foreach ($errors->get('month') as $message)
                <div class="invalid-feedback">{{ $message }}</div>
            @endforeach
        @endif
    </div>
    <div class="form-group">
        {{ Form::label('year', 'Год', ['class'=>'small text-muted font-italic']) }}
        {{ Form::select('year', $years, null, array('class' => 'form-control'. ($errors->has('year') ? ' is-invalid' : ''))) }}
        @if($errors->has('year'))
            @foreach ($errors->get('year') as $message)
                <div class="invalid-feedback">{{ $message }}</div>
            @endforeach
        @endif
    </div>
    <div class="form-group">
        {{ Form::label('value', 'Значение', ['class'=>'small text-muted font-italic']) }}
        {{ Form::number('value', null, array('step'=>'0.01', 'class' => 'form-control'. ($errors->has('value') ? ' is-invalid' : '' ))) }}
        @if($errors->has('value'))
            @foreach ($errors->get('value') as $message)
                <div class="invalid-feedback">{{ $message }}</div>
            @endforeach
        @endif
    </div>
    <button type="submit" id="submit" class="btn btn-success mt-3" title="Save"><i class="fa fa-save fa-fw"></i></button>

{{ Form::close() }}

@endsection
