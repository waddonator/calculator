@extends('layouts.admin')

@section('content')

{{ Form::open(array('url' => 'dashboard/units')) }}
    <h1 class="mb-5">{{trans('calculator.units')}} - {{trans('calculator.create')}}</h1>
    <div class="form-group">
        {{ Form::label('name', 'Название', ['class'=>'small text-muted font-italic']) }}
        {{ Form::text('name', null, array('class' => 'form-control'. ($errors->has('name') ? ' is-invalid' : '' ))) }}
        @if($errors->has('name'))
            @foreach ($errors->get('name') as $message)
                <div class="invalid-feedback">{{ $message }}</div>
            @endforeach
        @endif
    </div>
    <div class="form-group">
        {{ Form::label('visible', 'Видимость', ['class'=>'small text-muted font-italic']) }}
        <select class="form-control" name="visible">
            <option value="0">Нет</option>
            <option value="1">Да</option>
        </select>
    </div>
    <div class="form-group">
        {{ Form::label('slug', 'Слаг', ['class'=>'small text-muted font-italic']) }}
        {{ Form::text('slug', null, array('class' => 'form-control'. ($errors->has('slug') ? ' is-invalid' : '' ))) }}
        @if($errors->has('slug'))
            @foreach ($errors->get('slug') as $message)
                <div class="invalid-feedback">{{ $message }}</div>
            @endforeach
        @endif
    </div>
    <div class="form-group">
        {{ Form::label('image', 'Изображение', ['class'=>'small text-muted font-italic']) }}
        <div class="input-group mb-3">
            {{ Form::text('image', null, ['id' => 'image', 'class' => 'form-control' . ($errors->has('image') ? ' is-invalid' : '')]) }}
            <div class="input-group-append">
                <button data-toggle="modal" href="javascript:;" data-target="#images" class="btn btn-primary" type="button"> ... </button>
            </div>
        </div>
        @if($errors->has('image'))
            @foreach ($errors->get('image') as $message)
                <div class="form-control-feedback">{{ $message }}</div>
            @endforeach
        @endif
    </div>
    <div class="form-group">
        {{ Form::label('hint', 'Подсказка', ['class'=>'small text-muted font-italic']) }}
        {{ Form::textarea('hint', null, ['class' => 'form-control' . ($errors->has('hint') ? ' is-invalid' : '')]) }}
        @if($errors->has('hint'))
            @foreach ($errors->get('hint') as $message)
                <div class="invalid-feedback">{{ $message }}</div>
            @endforeach
        @endif
    </div>
    <button type="submit" id="submit" class="btn btn-success mt-3" title="Save"><i class="fa fa-save fa-fw"></i></button>

{{ Form::close() }}

<div id="images" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <iframe width="800" height="600" src="/filemanager/dialog.php?type=1&field_id=image&relative_url=1&akey=EGDT3dyjsMFxYV865EasV6e5RHkRXDCas8r7t2mv" frameborder="0" style="overflow: scroll; overflow-x: hidden; overflow-y: scroll; "></iframe>
        </div>
    </div>
</div>

@endsection
